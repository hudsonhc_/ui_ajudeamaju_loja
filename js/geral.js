$(function(){



	/*****************************************
		SCRIPTS PÁGINA INICIAL
	*******************************************/
	//CARROSSEL BANNER CATEGORIA
	$("#carrosselBannerCategoria").owlCarousel({
		items : 1,
        dots: true,
        loop: false,
        lazyLoad: true,
        mouseDrag:true,
        touchDrag: true,	       
	    autoplayTimeout:5000,
	    autoplayHoverPause:true,
	    smartSpeed: 450,

	    //CARROSSEL RESPONSIVO
	    //responsiveClass:true,			    
 //        responsive:{
 //            320:{
 //                items:1
 //            },
 //            600:{
 //                items:2
 //            },
           
 //            991:{
 //                items:2
 //            },
 //            1024:{
 //                items:3
 //            },
 //            1440:{
 //                items:4
 //            },
            			            
 //        }		    		   		    
	    
	});

	//CARROSSEL BANNER INICIAL
	$("#carrosselBannerInicial").owlCarousel({
		items : 1,
        dots: true,
        loop: false,
        lazyLoad: true,
        mouseDrag:true,
        touchDrag: true,	       
	    autoplayTimeout:5000,
	    autoplayHoverPause:true,
	    smartSpeed: 450,

	    //CARROSSEL RESPONSIVO
	    //responsiveClass:true,			    
 //        responsive:{
 //            320:{
 //                items:1
 //            },
 //            600:{
 //                items:2
 //            },
           
 //            991:{
 //                items:2
 //            },
 //            1024:{
 //                items:3
 //            },
 //            1440:{
 //                items:4
 //            },
            			            
 //        }		    		   		    
	    
	});

	//CARROSSEL PRODUTOS RELACIONADOS-PG PRODUTO
	$("#carrosselRelacionados").owlCarousel({
		items : 3,
        dots: true,
        loop: false,
        lazyLoad: true,
        mouseDrag:true,
        touchDrag: true,	       
	    autoplayTimeout:5000,
	    autoplayHoverPause:true,
	    smartSpeed: 450,

	    //CARROSSEL RESPONSIVO
	    responsiveClass:true,			    
        responsive:{
            320:{
                items:1
            },
            600:{
                items:2
            },
           
            991:{
                items:2
            },
            1024:{
                items:3
            },
            1440:{
                items:4
            },

        }		    		   		    
	    
	});

	$( ".produtosCategoria ul .produto figure .imagemDestaqueProduto" ).mouseenter(function(e){
       let imgFoco = $(this).attr("data-imgFoco");
       $(this).attr("src",imgFoco);

    }).mouseleave(function(e){
        let imgPadrao = $(this).attr("data-imgPadrao");
        $(this).attr("src",imgPadrao);
    });

    $( ".listaDeProdutosInicial ul li figure img" ).mouseenter(function(e){
       let imgFoco = $(this).attr("img-foco");
       $(this).attr("src",imgFoco);

    }).mouseleave(function(e){
        let imgPadrao = $(this).attr("img-padrao");
        $(this).attr("src",imgPadrao);
    });

    $( ".carrosselProdutosRelacionados .carrosselRelacionados .item figure img" ).mouseenter(function(e){
       let imgFoco = $(this).attr("img-foco");
       $(this).attr("src",imgFoco);

    }).mouseleave(function(e){
        let imgPadrao = $(this).attr("img-padrao");
        $(this).attr("src",imgPadrao);
    });

    var userFeed = new Instafeed({
       get: 'user',
       userId: '431966971',
       clientId: 'c7c6b34f76c04fd695e16fd6b5769c4f',
       accessToken: '9dbaad16b27b43a4bcd7b132e827fa2e',
       resolution: 'standard_resolution',
       template: '<a href="{{link}}" target="_blank" id="{{id}}"><div class="itemInstagram" style="background:url({{image}})"><small class="likeComments"><span class="likes">{{likes}}</span><span class="comments">{{comments}}</span></small></div></a>',
       sortBy: 'most-recent',
       limit: 8,
       links: false
     });
     userFeed.run();
		
});